#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_tcp350
#
require vac_ctrl_tcp350,1.5.1


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${vac_ctrl_tcp350_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: MEBT-010:Vac-VEPT-01100
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_ctrl_tcp350_moxa.iocsh", "DEVICENAME = MEBT-010:Vac-VEPT-01100, IPADDR = moxa-vac-mebt.tn.esss.lu.se, PORT = 4007")

#
# Device: MEBT-010:Vac-VPT-01100
# Module: vac_ctrl_tcp350
#
iocshLoad("${vac_ctrl_tcp350_DIR}/vac_pump_tcp350_vpt.iocsh", "DEVICENAME = MEBT-010:Vac-VPT-01100, CONTROLLERNAME = MEBT-010:Vac-VEPT-01100")
